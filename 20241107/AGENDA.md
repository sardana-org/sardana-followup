# Agenda for the Sardana Follow-up Meeting - 2024/11/07

To be held on Thursday 2024/11/07 at 14:00

Organizer: Teresa @ DESY

## Agenda

1. Operation highlights, user feedback, issues, etc. - Round table
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS

2. Review of pending points from previous meetings
    - sardana-icepap project in the sardana-org.

3. Overview of current developments / MR distribution
    - Hotfix 3.5.1
    - Sardana configuration improvements (SEP20)
    - Shutter integration (SEP21)
    - Fix invalid escape sequence warnings ([!2028](https://gitlab.com/sardana-org/sardana/-/merge_requests/2028)).
    - entryname and dataname in showscan customizable ([!2018](https://gitlab.com/sardana-org/sardana/-/merge_requests/2018))
    - Type hinting in Sardana ([!1924](https://gitlab.com/sardana-org/sardana/-/merge_requests/1924), [#1869](https://gitlab.com/sardana-org/sardana/-/issues/1869))
    - Set/reset ScanID from expconf ([#1967](https://gitlab.com/sardana-org/sardana/-/issues/1967)), ([MR](https://gitlab.com/sardana-org/sardana/-/merge_requests/2037))
    - Add "epoch" (#E) in SPEC recorder header ([!1356](https://gitlab.com/sardana-org/sardana/-/merge_requests/1356)
    
3. Jan25 release

4. Review of oldest 15 issues in the backlog
    - Next to review: [#156](https://gitlab.com/sardana-org/sardana/-/issues/156)

5. AOB
   - MAX IV visit to ALBA late november - agenda?
   - Next meeting?
