Minutes for the Sardana Follow-up Meeting - 2024/11/07

HEld on Thursday 2024/11/07 at 14:00

Organizer: Teresa @ DESY

Participants: 
- Johan (MAX IV)
- Michael (MBI)
- Michal (SOLARIS)
- Wojciech (SOLARIS)
- Jordi (ALBA)
- Oriol (ALBA)
- Teresa (DESY)


##Minutes

1.    Operation highlights, user feedback, issues, etc. - Round table
       - ALBA
         Nothing
       - DESY
         Nothing      
       - MAXIV
         Nothing
       - MBI Berlin
         Nothing
       - SOLARIS
         Need the MR ([!2022](https://gitlab.com/sardana-org/sardana/-/merge_requests/2022))
	 , created by them. Oriol will check it. If not merged soon they will create a
	 patch for running at Solaris.
	 
2.    Review of pending points from previous meetings
       - sardana-icepap project in the sardana-org.
         Still not done. Roberto will work on that.

3.    Overview of current developments / MR distribution
       - Hotfix 3.5.1
         It is still not done, there are three MR related to that, one aproved and the
	 other ones close to it. MaxIV has made them.
	 It is about the controllers state. Jordi and Oriol will try to check the MRs and
         create the Hotfix..
       - Sardana configuration improvements (SEP20)
         There is one MR related to it ([!2036](https://gitlab.com/sardana-org/sardana/-/merge_requests/2036)) fixing a problem found with MG. Review is pending.
	 Another old MR ([!1989](https://gitlab.com/sardana-org/sardana/-/merge_requests/1989)) is also pending, and ([!2025](https://gitlab.com/sardana-org/sardana/-/merge_requests/2025)) needs some discussion. Alba will take care.
       - Shutter integration (SEP21)
         Nothing new.
       - Fix invalid escape sequence warnings [!2028](https://gitlab.com/sardana-org/sardana/-/merge_requests/2028)).
         Nothing new.
       - entryname and dataname in showscan customizable ([!2018](https://gitlab.com/sardana-org/sardana/-/merge_requests/2018))
         Reviewed by Oriol. It is ready to merge when documented.
       - Type hinting in Sardana ([!1924](https://gitlab.com/sardana-org/sardana/-/merge_requests/1924), [#1869](https://gitlab.com/sardana-org/sardana/-/issues/1869))
         It was merged but contains some discussion that would be done in a related issue created for that purporse ([#1869](https://gitlab.com/sardana-org/sardana/-/issues/1869)).
       - Set/reset ScanID from expconf ([#1967](https://gitlab.com/sardana-org/sardana/-/issues/1967)), ([MR](https://gitlab.com/sardana-org/sardana/-/merge_requests/2037))
         Created by Wojciech. He had some doubts about what ScanID should be, the current one or the new one. It was decided to set in the GUI the ScanID corresponding to the next scan,
	 this will be explicitly written in the GUI, since the ScanID environment variable
	 indicates actually the ID of the last scan.
       - Add "epoch" (#E) in SPEC recorder header ([!1356](https://gitlab.com/sardana-org/sardana/-/merge_requests/1356))
         It is an old MR. In spec #E is in the file header, but there is not file header
	 in the sardana files, but only scan header. It has to be discussed.
	 Perhaps can be added as a custom field in the scan header.
	 The way implemented in the MR is not wanted, the MR will be kept open,
	 but the implementation has to be changed.
 

4.   Jan25 release
         Everybody who is interested in having a MR added, should put it in the Jan25 Milestones.
	 The Hotfix 3.5.1 will be still done independently before the release.

5.    Review of oldest 15 issues in the backlog
       - Next to review: #156
       Skipped, urgent issues should be put in the agenda next time.

6.    AOB
       - MAX IV visit to ALBA late november - agenda?
       	 MaXIV will visit Alba on 27 November. They want to know if an agenda should
	 be prepared. The Shutter and synchronization controllers will be reviewed.
	 Also redis could be seen.
       - Next meeting?
         12.12 by MaxIV
