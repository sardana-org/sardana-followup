# Minutes for the Sardana Follow-up Meeting - 2024/04/18

Held on Thursday 2024/04/18 at 14:00

Organizer: Jordi, Oriol, Zibi @ ALBA

Participants: Teresa, Michael, Benjamin, Michal, Zibi, Johan, Vanessa, Carla, Jordi, Oriol

## Minutes

1. sardana-icepap project in the sardana-org?
    - MBI Berlin and DESY agree (other institutes already did before). It is mentioned that this movement will show the community that the projects are closer to the core, supported and in close collaboration with Sardana, which is good.
    - Next step: Communicate sardana-icepap developers to go on with the migration.

2. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
      - Nothing to highlight
    - DESY
      - Mesh scan motor position issue. Option to disable the sending to position of the slow motor when moving the fast one. MR pending. We decided to go for both options optional macro parameter and env var ([#1229](https://gitlab.com/sardana-org/sardana/-/issues/1229)).
      - Regression (in develop vs 3.4.4) of the on_stop/on_abort propagation (ctrl+c) out of the `mv` macro when it is inside a loop ([#1949](https://gitlab.com/sardana-org/sardana/-/issues/1949)) 
      - Start changing Debian for Ubuntu. Sardana does not work with python 3.12 (default in Ubuntu 24.04).
    - MAXIV
      - Problem when restarting device from jive. Memorized attributes are lost. Already a fix 8 months ago ([#1867](https://gitlab.com/sardana-org/sardana/-/issues/1867)) but not released. MAX-IV created a local hotfix in his Sardana version.
      - Sardana CLI and server case sensitiveness issue in Win/Mac: [#1950](https://gitlab.com/sardana-org/sardana/-/issues/1950). Agreed to rename the CLI. Possible name to be discussed in the issue (sardanacli, sardanactl, sardanacmd, sardana-cli,...).
      - Highlight: First test of meshct with trajectory (IcepapTrajectoryCtrl added recently to sardana-icepap). It worked but stil missing features. New scan class (based on GScan). 15% faster for now.
    - MBI Berlin
      - Asked about the status/stability/reliability of the Sardana config tool. Discussion if sardana config should be a separate package.
    - SOLARIS
      - Nothing to highlight.

3. Jan24 release status
    - Agreed to make `USE_NUMERIC_ELEMENT_IDS = False` by default in the new release. Main argument against it was that rename element has not been ported to non-numeric ids yet.
    - Revision of pending issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues. To be added to the milestone:
      - Change the CLI launcher (as discussed in point 2) [#1950](https://gitlab.com/sardana-org/sardana/-/issues/1950)
      - The CTRL-C (abort) regression [#1949](https://gitlab.com/sardana-org/sardana/-/issues/1949)
      - Relmac issue [#1877](https://gitlab.com/sardana-org/sardana/-/issues/1877)
    - Revision of MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests. To be added to the milestone:
      - Config issues [!1998](https://gitlab.com/sardana-org/sardana/-/merge_requests/1998)
      - Click-to-move in showscan [!1983](https://gitlab.com/sardana-org/sardana/-/merge_requests/1983). Pending to fix an issue with an extra event when clicking the move dialog and a stop move option.
      - Make fscan deterministic when integ_time is scalar [!1997](https://gitlab.com/sardana-org/sardana/-/merge_requests/1997) 
    - Reminder that ALBA and SOLARIS are release managers
    - Platforms for manual tests:
      - Centos7 - SOLARIS
      - Debian 11 & 12, Ubuntu 24.04 - DESY
      - Debian 10 - ALBA
      - conda (with the latest conda-forge dependencies) - MAX IV

4. Sardana Workshop satellite to Tango Meeting
    - Agreed on the date/time slot (Thursday afternoon after Tango meeting). Announcement can be done.
    - Possibility of remote participation
    - Clarify with Soleil organizers if registration (to the Tango meeting) is required for the workshop (also to access to Soleil premises). Consider people that may attend only to Sardana workshop and not Tango meeting. If no registration is needed, ask for email confirmation to have a estimation of the assistance.
    - Confirmed assistance on site (for coffee break):
      - ALBA 4
      - MAX-IV 3 or 4
      - MBI 1
      - Solaris 4
    - Preliminary agenda accepted: https://gitlab.com/sardana-org/sardana-followup/-/merge_requests/126.Accepted

5. Tango Meeting at SOLEIL - Should we submit a Sardana status talk?
    - Type of presentation: Ideally talk with no demo (demos on the workshop).
    - Use collaborative Google Slides document.
    - Possible topics:
      - Jan24 release - if ready
      - Redis recorder
      - multiple files config
      - Continuous Scans Workshop in SOLARIS
      - shutter design controller - if ready
      - major bugfixes summary
      - interactions with tango ecosystem
        - fully dynamic attributes
        - reconfig via admin's device DevRestart command
        - starter issue to run servers with composed commands
      - Show sardana-tango and sardana-limaccd as they are related with Tango.  May be interesting for people new to Sardana.
    - Call for volunteers as speakers, to decide in the next follow-up.

6. Sardana incompatible with Python 3.12. Drop support to Python 3.5?
    - https://gitlab.com/sardana-org/sardana/-/issues/1914
    - Define a version policy, similar as PyTango or NumPy (see issue)
    - Add a page in Sardana docs with supported Python versions.
    - Drop Python 3.5 support after Sardana 3.5 (min. version will be Python 3.7 or 3.9 after consulting with ALBA and Solaris).

7. Review pending points from the previous meetings
    - From last meeting
      - [ ] tango serialization monitor acquiring timeout -> No updates
      - [X] continuous scans improvements developments dedicated meeting -> With the output, SEP21 was prepared. Organization of a follow-up to discuss the SEP.
    - From previous meetings
      - [ ] Sardana Users Training -> No updates. ALBA will prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.

8. Overview of current developments / MR distribution
    - Sardana configuration improvements (SEP20) -> Next step is the environment (not for the release, maybe future hotfix?)
    - Shutter integration (SEP21).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) -> Ideally fix in taurus. It is advancing.    
    - Issues already discussed in other sections: [#1948](https://gitlab.com/sardana-org/sardana/-/issues/1948), [#1946](https://gitlab.com/sardana-org/sardana/-/issues/1946), [#1950](https://gitlab.com/sardana-org/sardana/-/issues/1950), [#1229](https://gitlab.com/sardana-org/sardana/-/issues/1229)
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942) -> No time to go deep on this.
    - Not urgent but would be good to resurrect type hinting project after the release.
    
9. Review of the oldest 15 issues in the backlog
    - Skipped. It is proposed to move this section higher in the agenda for next meeting.

10. AOB
    - sardana followups advertised on [tango indico](https://indico.tango-controls.org/)? -> Skipped for next meeting.
    - Next meeting: Thursday May 16th at 14h. Organizer: DESY.
