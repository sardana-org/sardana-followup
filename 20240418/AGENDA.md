# Agenda for the Sardana Follow-up Meeting - 2024/04/18

Held on Thursday 2024/04/18 at 14:00

Organizer: Jordi, Oriol, Zibi @ ALBA

## Agenda

1. sardana-icepap project in the sardana-org?
   - This idea came up on the last sardana-icepap collaboration meeting between ALBA, MAXIV and SOLARIS.
   - Its purpose is to foster collaboration and simplify management of project.
   - In the future we could extend it to other projects widely used in the community e.g. sardana-tango

2. Operation highlights, user's feedback, issues, etc. - Round table
   - ALBA
   - DESY
   - MAXIV
   - MBI Berlin
   - SOLARIS
   - ...

3. Jan24 release status
   - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
   - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests
   - ALBA and SOLARIS are release managers
     - we will meet and distribute [work](https://gitlab.com/sardana-org/sardana/-/wikis/How-to-release-Sardana)
   - platforms for manual tests:
     - Centos7 - SOLARIS
     - Debian 11 and 12 - DESY
     - Debian 10 - ALBA
     - conda (with the latest conda-forge dependencies) - MAX IV

4. Sardana Workshop satellite to Tango Meeting
   - Next steps:
     - Officially announce the event on the Indico site of the Tango Meeting and on the mailing lists.
     - Order a coffee break for the afternoon, how many participants?
     - Plan the agenda of the meeting and redistribute the topics to prepare the sessions. 
       - https://gitlab.com/sardana-org/sardana-followup/-/merge_requests/126

5. Tango Meeting at SOLEIL - Should we submit a Sardana status talk?
     - Jan24 release - if ready
     - redis recorder
     - multiple files config
     - Continuous Scans Workshop in SOLARIS
     - shutter design controller - if ready
     - major bugfixes summary
     - interactions with tango ecosystem
       - fully dynamic attributes
       - reconfig via admin's device DevRestart command
       - starter issue to run servers with composed commands

6. Sardana incompatible with Python 3.12. Drop support to Python 3.5?
   - https://gitlab.com/sardana-org/sardana/-/issues/1914

7. Review pending points from the previous meetings

   - From last meeting
     - [ ] tango serialization monitor acquiring timeout 
       - custom motor controller reading/setting attributes of its own motor via device proxy in a callback method
       - there are corner cases where locks on a tango layer happen 
       - to study more and start an issue to explain in detail and start discussion
       - MP will prepare a recipe to reproduce this on a dummy elements
     - [ ] continuous scans improvements developments dedicated meeting 
       - there will be a poll to select a date
   - From previous meetings
     - Sardana Users Training
       - [ ] It is planned to have a meeting after we define what we will plan.
         Solaris and MaxIV will prepare requirements. 
         Michal coordinator. We will call for a dedicated meeting.
         - all the institutes (MAX IV, ALBA, SOLARIS) should gather their internal feedback from the users of their needs
         - later we can share the results and see what are the common points

8. Overview of current developments / MR distribution
    - Sardana configuration improvements (SEP20)
    - Shutter integration (SEP21) 
    - fscan should be deterministic if integ_time is a scalar - [#1948](https://gitlab.com/sardana-org/sardana/-/issues/1948)
    - meshct (rscanct and a%scanct) improvements and CTScan set meas. grp. NbStarts - [#1946](https://gitlab.com/sardana-org/sardana/-/issues/1946)
    - Sardana server and cli case sensitive issue - [#1950](https://gitlab.com/sardana-org/sardana/-/issues/1950)
    - mesh scans with virtual motors sharing physical motors - [#1229](https://gitlab.com/sardana-org/sardana/-/issues/1229)
    - add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984)
    - run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
    - For sure I forgot something, please add it here...
    
9. Review of the oldest 15 issues in the backlog

10. AOB
   - sardana followups advertised on [tango indico](https://indico.tango-controls.org/)? (similar to taurus followups) 
     - good idea, but we need someone to grant us permissions to create events there
     - maybe add `info@tango-controls.org` mailing group (similar to taurus followups) to email invitation
   - Next meeting?
