# Agenda for the Sardana Follow-up Meeting - 2024/08/22

Held on Thursday 2024/08/22 at 14:00

Organizer: Michal @ SOLARIS

## Agenda

1. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS
 
2. Review pending points from the previous meetings
    - From previous meeting
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.
      - can we drop support for some old python versions? 
        - 3.5 can be dropped in next release, but what about 3.6? 
          - Solaris may be still stuck on 3.6 due to CentOS7.
      - sardana-icepap project in the sardana-org (nobody involved was present last time)
      - Problem with logging to stdout ([#1973](https://gitlab.com/sardana-org/sardana/-/issues/1973))

7. Overview of current developments / MR distribution.
    - Sardana configuration improvements (SEP20): status of environment implementation
    - Shutter integration (SEP21) [!1995](https://gitlab.com/sardana-org/sardana/-/merge_requests/1995).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
    - Problem with latency time not being applied to measurement groups ([#1975](https://gitlab.com/sardana-org/sardana/-/issues/1975), [!2020](https://gitlab.com/sardana-org/sardana/-/merge_requests/2020))
    - Scanstats ideas ([#1974](https://gitlab.com/sardana-org/sardana/-/issues/1974), [!2022](https://gitlab.com/sardana-org/sardana/-/merge_requests/2022))
    - ?

6. Review of the oldest 15 issues in the backlog
   - Next up: issue #150

8. AOB
   - Next meeting?
