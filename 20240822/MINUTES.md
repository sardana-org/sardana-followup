# Minutes for the Sardana Follow-up Meeting - 2024/08/22

Held on Thursday 2024/08/22 at 14:00

Organizer: Michal @ SOLARIS

Participants: Michal (SOLARIS), Oriol, Jordi, (ALBA), Vanessa (MAX IV), Michael (MBI), Teresa (DESY)

## Minutes

1. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
      - no issues to report
    - DESY
      - no issues to report
    - MAXIV
      - startup this week
      - issue with measurement group consisting only tango attributes
      - Jordi confirmed that it is an already known issue
      - measurement group should have at least one ct channel (even dummy one), wrong configurations should generate error/warning
      - discussion will be continued in dedicated issue
    - MBI Berlin
      - no issues to report
    - SOLARIS
      - sardana migration postponed to winter shutdown (OS + Sardana version), for now only the new beamline is running Sardana 3.5 on Alma9
 
2. Review pending points from the previous meetings
    - From previous meeting
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.
        - Taurus will be first
      - can we drop support for some old python versions? 
        - 3.5 can be dropped in next release, but what about 3.6? 
          - Solaris may be still stuck on 3.6 due to CentOS7.
            - after OS + Sardana version migration in winter shutdown 24 we will use at least 3.9 
      - sardana-icepap project in the sardana-org (nobody involved was present last time)
        - no update
      - Problem with logging to stdout ([#1973](https://gitlab.com/sardana-org/sardana/-/issues/1973))
        - found and reported walkaround, maybe in the future it will be solved in Tango (log rotation)
      
3. Overview of current developments / MR distribution.
    - Sardana configuration improvements (SEP20): status of environment implementation
      - no update
    - Shutter integration (SEP21) [!1995](https://gitlab.com/sardana-org/sardana/-/merge_requests/1995).
      -  [!2021](https://gitlab.com/sardana-org/sardana/-/merge_requests/2021) is now ready to merge, feedback from the community is welcome
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
      - already changed in Taurus
      - could be probably closed
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
      - probably can be achieved by existing tools/macros
      - discussion in the issue
    - Problem with latency time not being applied to measurement groups ([#1975](https://gitlab.com/sardana-org/sardana/-/issues/1975), [!2020](https://gitlab.com/sardana-org/sardana/-/merge_requests/2020))
      - ready to be reviewed, feedback from the community is welcome
    - Scanstats ideas ([#1974](https://gitlab.com/sardana-org/sardana/-/issues/1974), [!2022](https://gitlab.com/sardana-org/sardana/-/merge_requests/2022))
      - ready to be reviewed, feedback from the community is welcome 

4. Review of the oldest 15 issues in the backlog
   - two issues reviewed:
     - [#151](https://gitlab.com/sardana-org/sardana/-/issues/151)
       - not sure if this is still relevant
       - test is necessary to confirm
     - [#153](https://gitlab.com/sardana-org/sardana/-/issues/153)
       - contact with Miquel on status and how to proceed
   - next to review is [#156](https://gitlab.com/sardana-org/sardana/-/issues/156)

5. AOB
   - NOBUGS 
      - talk for NOBUGS is being prepared
      - DESY is giving talk about "Daiquiri, blissdata, bluesky, and sardana"
        - there is an interest to hear more on the status on the next followup
        - Teresa will ask about it
   - Next meeting?
      Tentative date for the next meeting is 04.10.2024 at 11:00 (Friday!), organised by ALBA