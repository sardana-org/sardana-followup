# Minutes for the Sardana Follow-up Meeting - 2024/02/08

Held on Thursday 2024/02/08 at 14:00

Organizer: Teresa @ DESY

Participants: Oriol, Zibi, Jordi, Roberto (ALBA), Aureo, Vanessa (MAX-IV),
              Michal (Solaris), Alexander Kessler (HI-Jena, listenig,
	      they are using Sardana in his lab)

## Minutes

1. Sardana Redis recorder using blissdata 1.0.

    ESRF has prepared some code to be tested by DESY and ALBA, with more
    documentation. In this code Flint is  not requiring BLISS.
    Oriol has been working on that, for the moment he found everything
    fine and the documentation is better.
    It can be used with the Redis Sardana Recorder.
    There are few points to discuss with ESRF, but he thinks it can be
    adapted to Sardana quite well.
    One of these points is that ESRF is not using the NeXus application
    definitions. The reason they give for that is that this would
    require data from other sources, not only the data from the scan.
    In any case they publish all the information so the users could
    get the data and create by themselves new NeXus files with the
    application definitions.
    Zibi will reply to ESRF with the status and a conclusion of
    the tests in ALBA.
    We will then propose a date for the next meeting.
    

2. Sardana configuration improvements (SEP20).

   There were quite improvements on the sardana configuration tool and new MRs
   pending to be integrated.

   - https://gitlab.com/sardana-org/sardana/-/merge_requests/1968
     Still not tried by ALBA since they wanted to test first same small things.

   - A lot of work was done concerning the DOOR names, Zibi wanted to check it,
     there is already a MR.
     
   - Johan provided a MR
     https://gitlab.com/sardana-org/sardana/-/merge_requests/1981
     due to issues in Solaris, Michal will test it.
     One of the problem was about changes in the configuration of the
     MG (like the removing of "shape").
   
3. Operation highlights, user's feedback, issues, etc. - Round table

   - ALBA
     Problem in a beamline, loosing sign and offset of a motor after
     the migration. It happens only in one beamline.
     Other problem is the limits of the mesh, only used in one beamline.
     One motor has speed 0 and this is a problem for computing these limits,
     they create a hotfix for fixing it.
     Vanessa had the same problem, they put a minimum velocity to be used
     in case of 0 speed.
     There is a hotfix 3.4.4 solving this issue, it is integrated in develop.
     There is a feature for putting None in a motor if it is not wanted to
     be moved, it could be used but it is not reliable.
   - DESY
     Nothing to report.
   - MAXIV
     They are upgrading python.
     They had a problem in sardana, due to an old problem of cpp Tango, the problem is because
     Sardana uses a motor class for the same hardware and they
     need the same attribute with different type and format, this is not possible in
     Tango. Tango wants to make softer this rule, but it is not still on the roadmap for the
     close future. A work around is to rename the attributes.
     They have some other problems with the polling if there is a crash. They put it in Sherlock
     to get warnings about it.
     They also have problems with timeouts in MG making that the scan fails.
     They think in increasing the timeouts in the MG.
     It was in the start command of one detector. In Alba they try to divide the method if it
     takes more than 3 seconds, for example in prestart and start.
     In MaXIV, they use hooks and increase the timeouts for solving this.
     There was some work about it on:
     https://gitlab.com/sardana-org/sardana/-/issues/1721
     one can configure there the timeouts, even if it is still a workaround.
     In the prescan hook one could make the configuration at once if something takes too much time.
     They also have issues with fscan, Vanessa modified it for time resolved experiments,
     but it does not behave like Vanessa expected. She will open an issue, the problem could
     be related to the changes for
     deterministic scans, perhaps the fscan was not taken into account.
     They have implemented the hw orchestration.
     FMB monocromators acs motor controller, successfully commission of the macro,
     multiple energy edges scans + XRD in one shot
     trigger/gate controller that take start and stop motor trajectory
     3 more beamlines will come
   - MBI Berlin
     Michael could not assist to the meeting.
   - SOLARIS
     Not advance on trajectories for production. 
     Problems with configuration dump, they are reported: shape disappeared after update

4. Review pending points from the previous meetings

   - From last meeting
   - From previous meetings:
     - [x] Sardana roadmap: set milestone for the next release and assign short terms issues to it + add assignees
       The plan is to have a first internal meeting for trajectories and synchronization and after that
       have a meeting with the comunity
       
     - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab (no update x3)

5. Jan24 release status
   - issues: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-issues
   - MR: https://gitlab.com/sardana-org/sardana/-/milestones/14#tab-merge-requests

   Added MR to the scope
   Evaluate MR from Michael (MBI)

6. Sardana Users Training

   It is planned to have a meeting after we define what we will plan.
   Solaris and MaxIV will prepare requirements. 
   Michal coordinator. We will call for a dedicated meeting.
   
7. Overview of current developments / MR distribution

   Multi-device test context will use it at Alba for testing, at Solaris and MaxIV do not use it.
   We will create issue to PyTango
   MaxIV new project implement scan meshct to reduce dt and use trajectories with icepap. 
   Current meshct 1.2 s to switch to the next line in meshct
   meshct with trajectory drawing an area on an image 
   Maybe candidate for the sardana-icepap meeting
   Spock prompt
   
8. Review of the oldest 15 issues in the backlog

9. AOB

   Prepare meetings to coordinate collaboration on general sardana-controller.
   Next Friday 16th MaxIV, Alba, Solaris will have a meeting. 

   - Next meeting:
   14.03.24, organized by Michal (Solaris)
