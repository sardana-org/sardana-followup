# Agenda for the Sardana Follow-up Meeting - 2024/10/04

To be held on Friday 2024/10/04 at 11:00

Organizer: Oriol, Jordi, Zibi @ ALBA

## Agenda

1. Operation highlights, user feedback, issues, etc. - Round table
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS

2. Review of pending points from previous meetings
    - sardana-icepap project in the sardana-org. Any updates? This point seems stalled. If nothing is missing, decide who will take care of it and how to proceed.

3. Overview of current developments / MR distribution
    - Hotfix 3.5.1
        - Element's State.Running equivalent to State.Moving in measurement group state machine ([!2027](https://gitlab.com/sardana-org/sardana/-/merge_requests/2027), [#1982](https://gitlab.com/sardana-org/sardana/-/issues/1982))
        - Fix KeyError when reloading macro with mixed case ([!2026](https://gitlab.com/sardana-org/sardana/-/merge_requests/2026))
    - Sardana configuration improvements (SEP20)
        - status of environment implementation
        - Handle missing type information ([!2025](https://gitlab.com/sardana-org/sardana/-/merge_requests/2025))
    - Shutter integration (SEP21) [!1995](https://gitlab.com/sardana-org/sardana/-/merge_requests/1995). 
        - Multiple Synch Descriptions ([!2021](https://gitlab.com/sardana-org/sardana/-/merge_requests/2021), [!2030](https://gitlab.com/sardana-org/sardana/-/merge_requests/2030))
        - Draft: Shutter object ([!2029](https://gitlab.com/sardana-org/sardana/-/merge_requests/2029))
    - Fix invalid escape sequence warnings ([!2028](https://gitlab.com/sardana-org/sardana/-/merge_requests/2028)). Ready to merge?
    - entryname and dataname in showscan customizable ([!2018](https://gitlab.com/sardana-org/sardana/-/merge_requests/2018)). Ready to merge after adding documentation.
    
3. Jan25 release
    - Should we start planning it?
    - Taurus 5.2.0 release in Oct24. Testing in Sardana.

4. Review of oldest 15 issues in the backlog
    - Next to review: [#156](https://gitlab.com/sardana-org/sardana/-/issues/156)

5. AOB
   - Next meeting?
