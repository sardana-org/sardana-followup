# Minutes for the Sardana Follow-up Meeting - 2024/10/04

Held on Friday 2024/10/04 at 11:00

Organizer: Oriol @ ALBA

Participants: 
- Teresa (DESY)
- Johan (MAX IV)
- Vanessa (MAX IV)
- Michael (MBI)
- Michal (SOLARIS)
- Jordi (ALBA)
- Oriol (ALBA)

## Minutes

1. Operation highlights, user feedback, issues, etc. - Round table
    - ALBA
      - One beamline has been running Sardana 3.5 since early December. No issues. It will be installed in more beamlines during the October shutdown, potentially deployed with Conda (up to now was in Debian 10)
      - Pickle Protocol issue ([#1933](https://gitlab.com/sardana-org/sardana/-/issues/1933)) with MacroServer environment was detected on one beamline currently in commissioning that has Sardana in both system and Conda (for specific Shutter developments). This issue is fixed in Taurus ([!1299](https://gitlab.com/taurus-org/taurus/-/merge_requests/1299)) and will be available in the next release. A local patch has been applied in the meantime.
    - DESY
      - No issues to report
      - Regarding the Pickle Protocol issue originally reported by them, they have also applied a local hotfix.
    - MAXIV
      - Upgraded to Sardana 3.5 in more beamlines with a local hotfix for controller states ([!2027](https://gitlab.com/sardana-org/sardana/-/merge_requests/2027)) and everything is running smoothly.
      - A new person will be involved in Redis-related developments.
    - MBI Berlin
      - Using Sardana 3.5 on most instruments.
      - There is an issue with latency time in the Measurement Group when using software synchronization. When latency changes between two measurements, the MG does not update it properly. An issue in Sardana will be created.
    - SOLARIS
      - Sardana 3.5 is in use on one beamline in commissioning. No extensive use yet, but everything is running fine so far. Other beamlines will be upgraded during the next winter shutdown.

2. Review of pending points from previous meetings
    - sardana-icepap project in the sardana-org. Any updates? This point seems stalled. If nothing is missing, decide who will take care of it and how to proceed.
      - ALBA will attempt to migrate the repository from GitHub to GitLab while keeping all the history. Maybe it could be a good idea to document the process or any issues encountered to help with future migrations of other plugin repositories.

3. Overview of current developments / MR distribution
    - Hotfix 3.5.1 including [#1982](https://gitlab.com/sardana-org/sardana/-/issues/1982) and [!2026](https://gitlab.com/sardana-org/sardana/-/merge_requests/2026)
      - It was previously discussed that the [hotfix process](https://gitlab.com/sardana-org/sardana/-/wikis/How%20to%20hotfix%20Sardana) could be simplified. We could review this when preparing the hotfix. It is unclear who will create the hotfix release, possibly MAX IV, who initially found the issues.
      - We agreed that making the `MOVING` and `RUNNING` states equivalent would reduce confusion. There was also a discussion about improving the Controller documentation (state machine dependencies, return values, etc.). The Motor Controllers have a table summarizing the possible states, and it was suggested to add something similar for ct, 0D, 1D & 2D controllers.
      - The documentation can be added after the hotfix. We will create an issue or MR for this.
    - Sardana configuration improvements (SEP20)
      - The environment implementation status is still pending. Zibi is finishing the documentation for the Macroserver environment, which will be available soon.
      - Handle missing type information ([!2025](https://gitlab.com/sardana-org/sardana/-/merge_requests/2025)). Maybe someone can take a look and give opinions.
    - Shutter integration (SEP21)
      - Multiple Synch Descriptions ([!2021](https://gitlab.com/sardana-org/sardana/-/merge_requests/2021)) is no longer a draft and is ready for review.
      - Redis (BlissData) implementation in the core ([!2030](https://gitlab.com/sardana-org/sardana/-/merge_requests/2030)). We need to create an individual MR and refactor it to be compatible with BlissData 2.0.
      - Shutter object ([!2029](https://gitlab.com/sardana-org/sardana/-/merge_requests/2029)). In draft. We need to discuss it further to ensure it meets the needs of the majority and covers the various use cases across institutes.
    - Fix invalid escape sequence warnings ([!2028](https://gitlab.com/sardana-org/sardana/-/merge_requests/2028)). Ready to merge.
    - entryname and dataname in showscan customizable ([!2018](https://gitlab.com/sardana-org/sardana/-/merge_requests/2018)). Ready to merge after adding documentation.
    - Johan mentioned work done a year ago regarding type hinting in Sardana. Around 50% has been completed, but finishing it would greatly improve code navigation and understanding. He proposes to revive this project and continue the work if somebody else is interested in participating ([!1924](https://gitlab.com/sardana-org/sardana/-/merge_requests/1924), [#1869](https://gitlab.com/sardana-org/sardana/-/issues/1869)). ALBA will review what has been done and see if they can help.

4. Jan25 release
    - Should we start planning it?
      - We often start too late planning milestones, so it would be good to begin planning now.
      - Each institute can propose some MRs or issues to include in the Jan25 release, and we will discuss them in the next follow-up.
      - As for including a new Taurus release in Sardana, we will do so when preparing the new release and test it during the release testing phase. However, we could start using it in our testing environments as soon as it is available for early detection of potential issues.

5. Review of oldest 15 issues in the backlog -> skipped!
    - Next to review: [#156](https://gitlab.com/sardana-org/sardana/-/issues/156)

6. AOB
    - Sardana Training:
      - MAX IV mentions demand for training from BL staff. It would also be useful to create "standard" training materials.
      - The plan was to create a poll to identify users' interests and specific use cases to prepare more focused trainings if necessary.
      - ALBA is preparing a Taurus training for the end of this year so they have limited time to work on Sardana materials right now. It could be planned for next year.
    - Vanessa proposes to celebrate another Bug Squashing Party, as it is a good oportunity to get more familiar with the Sardana code.
    - During the NOBUGS conference, it was suggested to hold a second edition of the Sardana Continuous Scans workshop. Since some work has already begun, discussing different points of view could be useful.
    - We could merge the last two proposals in one presential event (e.g., 2-day workshop + 1-day bug squashing). To be refined for next follow-up.
    - Next meeting: **2024-11-07 at 14h**, organized by DESY
