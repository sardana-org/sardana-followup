# Agenda for the Sardana Follow-up Meeting - 2022/06/02
To be held on Thursday 2022/06/02 at 14:00

## Agenda

1. Sardana Status @ Tango Meeting 2022
   
   - Speakers Teresa?, Michal?, Both?; Help from Johan and Zibi
   - Outline?
   - Submitting a proposal on Indico and asking for a time slot before 16:00.
  
2. Sardana release Jul22
   
   - Release Managers: DESY & MAXIV
   - Version was bumped in develop branch by Zibi, and changes drafted in CHANGELOG.
   - Versions were installed at BL16 at ALBA and tested. Issues are being fixed.
   - What about more frequent releases in the future after canary releases in production environments?
   - Testing platforms?
  
3. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   
   - MVP selection
   - technical meeting on 9/05/2022
  
4. Urgent user problems/issues - Round table
   
    - SOLARIS
    - MAXIV
    - DESY
    - ALBA
      - BL16 and BL22 suffer for passing wrong target position in move of MotorGroup in continuous scan
      - BL04 and BL16 require Spec file header with #E (epoch) and #F (file abs path)
      - Elmo motor question still pending, no answer from MAXIV
      - 
    - MBI
    - ...

5. Review pending points from the previous meetings

   - From last meeting:
     - Discussion on limit switches
       - [ ] all issues/ideas should be documented in one place, Zibi will document it in [#159](https://gitlab.com/sardana-org/sardana/-/issues/159) and this will be the main place for discussion on this matter
       - [ ] issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641) -  Daniel will propose a MR for the naming
     - Sardana Configuration Tool
       - [ ] analyze configuration points (i.e environment - Zibi will prepare it)
        
   - From previous meetings:
     - [ ] Reported by MAXIV: Issue with extra axis attributes on pseudo motor not causing change events on position
           https://gitlab.com/sardana-org/sardana/issues/1693 <- related
           - no update x 1
     - [X] Other 3.2 issue, Tango attribute controller has a very long timeout (can be 100000 s) for attributes that don't implement 
           the "MOVING" quality, that can can prevent aborting macros. It can be configured with a speed attribute but may need 
           tuning per attribute. Proposed solution is to allow aborting.
           - no update x 2, Zibi will create an issue 
     - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
           a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
           Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
           example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
           - no update x 2
     - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
           - mentioned in round table
           - no update x 3, need to reproduce
     - [ ] Users can break Sardana by messing with configuration in Tango DB:
           - status of the diagnostics script:
              - [ ] Henrik wanted to register sherlock in the extras catalogue
              - no update x 3
     - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
           - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
           - no update x 3
          
6. Overview of current developments / MR distribution
   - `reconfig` macro, in order to work with single Sardana server, requires [cppTango!907](https://gitlab.com/tango-controls/cppTango/-/merge_requests/907)
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)
   - Draft: allow to exec new macros after stopping a macro [!1754](https://gitlab.com/sardana-org/sardana/-/merge_requests/1754)
   - Fix Check against the last event value of integration time in MeasurementGroup [!1751](https://gitlab.com/sardana-org/sardana/-/merge_requests/1751)

7. AOB
   - Next meeting?
