# Minuets for the Sardana Follow-up Meeting - 2022/06/02

Held on Thursday 2022/06/02 at 14:00

Present:
- Zibi (ALBA)
- Michal (SOLARIS)
- Teresa (DESY)
- Johan (MAXIV)

## Minutes

1. Sardana Status @ Tango Meeting 2022
   
   - Speakers Teresa?, Michal?, Both?; Help from Johan and Zibi
     - Teresa will make the talk alone. We will all help her with preparing the contents.
   - Outline?
     - Reminder of what Sardana does; why it is good?
     - Which institutes are using Sardana
     - SEP20 - Configuration Tool
     - Upcoming Sardana release 3.3 (Jul22 milestone)
   - Use Google Slides tool for collaborative development - slides hosted on Michal's account
   - Submitting a proposal on Indico and asking for a time slot before 16:00
  
2. Sardana release Jul22
   
   - Release Managers: DESY (Teresa) & MAXIV (Johan)
   - Milestone Jul22:
     - We selected MR to the release scope
     - Zibi will review some developments and try to close them
   - What about more frequent releases in the future after canary releases in production environments?
     - Version was bumped in develop branch by Zibi, and changes drafted in CHANGELOG.
     - Versions were installed at BL16 at ALBA and tested. Issues are being fixed.
     - Zibi will prepare a proposal of more frequent releases
   - Testing platforms?
     - Debian 9, 10, 11
     - CentOS 7
     - Conda (latest dependencies)
  
3. Sardana Configuration Tool - see [!1749](https://gitlab.com/sardana-org/sardana/-/merge_requests/1749)
   
   - MVP selection
     - ALBA vote is missing
     - So far GEN01, GEN05 and OPER01 have 3 votes; GEN02 and DBG03 have 2 votes; there are several with just 1 vote
   - We will try to use Requirements GitLab feature for describing and refining requirements
   - Technical meeting on 9/05/2022 - 1 hour only
  
4. Urgent user problems/issues - Round table
   
    - SOLARIS
      - IntegrationTime issue with multiple MacroServer instances
      - No other issues
    - MAXIV
      - No update (Johan had to leave earlier)
    - DESY
      - No urgent issues
    - ALBA
      - BL16 and BL22 suffer for passing wrong target position in move of MotorGroup in continuous scan
        - Zibi will create a an issue in Sardana and in PyTango
      - BL04 and BL16 require Spec file header with #E (epoch) and #F (file abs path)
      - Elmo motor question still pending, no answer from MAXIV      
    - MBI
      - Duplicate alias on same tango host - [#998](https://gitlab.com/sardana-org/sardana/-/issues/998)

5. Review pending points from the previous meetings

   - From last meeting:
     - Discussion on limit switches
       - [x] all issues/ideas should be documented in one place, Zibi will document it in [#159](https://gitlab.com/sardana-org/sardana/-/issues/159) and this will be the main place for discussion on this matter
         - Done.
       - [ ] issue raised about macro names for setting limits unification [#1641](https://gitlab.com/sardana-org/sardana/-/issues/1641) -  Daniel will propose a MR for the naming
         - No update
     - Sardana Configuration Tool
       - [x] analyze configuration points (i.e environment - Zibi will prepare it)
         - Done.
        
   - From previous meetings:
     - [ ] Reported by MAXIV: Issue with extra axis attributes on pseudo motor not causing change events on position
           https://gitlab.com/sardana-org/sardana/issues/1693 <- related
           - no update x 2
     - [X] Other 3.2 issue, Tango attribute controller has a very long timeout (can be 100000 s) for attributes that don't implement 
           the "MOVING" quality, that can can prevent aborting macros. It can be configured with a speed attribute but may need 
           tuning per attribute. Proposed solution is to allow aborting.
           - no update x 3, Zibi will create an issue 
     - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henry is working on that: sending
           a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
           Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
           example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
           - no update x 3
     - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
           - mentioned in round table
           - no update x **4**, need to reproduce
           - removing this point
     - [ ] Users can break Sardana by messing with configuration in Tango DB:
           - status of the diagnostics script:
              - [ ] Henrik wanted to register sherlock in the extras catalogue
              - no update x **4**
              - removing this point
     - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
           - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
           - no update x **4**
           - removing this point
          
6. Overview of current developments / MR distribution
   - `reconfig` macro, in order to work with single Sardana server, requires [cppTango!907](https://gitlab.com/tango-controls/cppTango/-/merge_requests/907)
     - Zibi will ping Daniel, if no reply, integrators will review.
   - 1D channels in SPEC header (storage files) [!1746](https://gitlab.com/sardana-org/sardana/-/merge_requests/1746)
     - Zibi will ping Daniel, if no reply, integrate it
   - Draft: allow to exec new macros after stopping a macro [!1754](https://gitlab.com/sardana-org/sardana/-/merge_requests/1754)
     - Zibi will make the commit avoiding on_stop() call of the macro executed after the first Ctrl+C
     - We think that it should be possible to abort the macro executed after the first Ctrl+C
   - Fix Check against the last event value of integration time in MeasurementGroup [!1751](https://gitlab.com/sardana-org/sardana/-/merge_requests/1751)
     - Michal will review it next week

7. AOB
   - Next meeting - 14.07.2022; chair by Teresa
   - Sardana Bug Squashing Party #2
     - Hybrid format (having only presential may be too hard to get many participants (last time we were almost 20))
     - Location: Barcelona? Krakow?
     - When:
       - tentative dates: end of July; September (depending on Teresa's holidays); October (10-14; 20-24)
