# Minutes for the Sardana Follow-up Meeting - 2022/03/03

To be held on Thursday 2022/03/03 at 14:00

Present:
- Johan (MAXIV)
- Zibi (ALBA)
- Michal (SOLARIS)
- Teresa (DESY)

## Agenda

1. Urgent user problems/issues - Round table

    - SOLARIS
      - Shutdown until now
    - MAXIV
      - Issue with extra axis attributes on pseudo motor not causing change events on position
        https://github.com/sardana-org/sardana/issues/1693 <- related
      - Sardana 3.2 is not compatible with python 3.10 
        conda installation broken unless older python version specified.
        This is fixed in develop, due to https://gitlab.com/sardana-org/sardana/-/merge_requests/1722
        Propose to release a "hotfix" version 3.2.1 https://gitlab.com/sardana-org/sardana/-/wikis/How-to-Hotfix-Sardana with this backported.
        Zibi will test a bit to see that there are no obvious issues with 3.10. Then we schedule creating the hotfix release.
    - DESY
        Busy with upgrade to Debian 10.
    - ALBA
      - Smaract motor controller required feature: dummy motor controller has some (old) error when setting accelleration velocity. Problems with order of restoring memorized attributes; tango does not define the order in which it restores them. The idea is to build on  https://gitlab.com/sardana-org/sardana/-/merge_requests/1730 and control the order of restoring attributes. This is urgent, Zibi will open an issue.
      - Motor related question from Guifre to MAXIV. Some scientists want to use an Elmo motor LAB Motion DB3.6 tango device from DESY (available in the tango ds registry). Question is, do we use it, and especially if we integrate it in sardana.
      - Sardana 3.2 upgrade, generally works fine but had some issue with timers. Old code that might be removed. Internally in measurement group there is a mapping of controllers to timer/monitor. There is also a "global" timer/monitor, this is for backwards compatibility, it does not actually do anything, Question to other institutes, if someone would be affected by removal. (E.g. scangui). Zibi will create an issue.
      - Other 3.2 issue, Tango attribute controller has a very long timeout (can be 100000 s) for attributes that don't implement the "MOVING" quality, that can can prevent aborting macros. It can be configured with a speed attribute but may need tuning per attribute. Proposed solution is to allow aborting.
    - ...

2. Review pending points from the previous meetings

    - From last meeting:
    
        - [X] ALBA: New requirement for continuous scans: configure the timeout for acquisition, now value fixed to 15 sec (after the movement, for
       avoiding scans hanging for ever). Roberto will create an issue and MR for that.
       https://gitlab.com/sardana-org/sardana/-/merge_requests/1742
       
        - [ ] Issue with configurationn of dynamic attributes, mostly on the tango layer. Henrik is working on that: sending
       a message at startup if there is a conflict in the configuration instead of waiting for error in run time.
       Problems with measurement groups that prevent things for starting, the error is on attributes. It happens for
       example for memorized attributes in IcePap. It will be checked again, and an issue will be created if needed.
       https://gitlab.com/sardana-org/sardana/-/merge_requests/1677
    
    - From previous meetings:
        - [ ] Zibi will set up meeting about Sardana configuration tool (discussion about solutions and text formats)
            - Will happen soon, ALBA is prioritizing this.
        
        - [x] `self.<macro name>` exceptions are uninformative.
            - looked into and agreed but on all levels of traceback it is uninformative 
            - issue in Sardana and description of the traceback loops needed, then discussion about how it should work
            - Issue created: https://gitlab.com/sardana-org/sardana/-/issues/1731

        - [ ] Scans using two different pools together experience hangs. Perhaps some cases are related to state attribute polling.
            - mentioned in round table
            
        - [X] Problem with pseudocounters of pseudocounters. They work when first created, but not later (e.g. after restart).
	      There is some problem with the order in which they get created.
            - Daniel's question, issue created, probably a bug https://gitlab.com/sardana-org/sardana/-/issues/823
            
        - [x] MacroServer startup hook (that would be called automatically after starting the server - an opposite to the at exit)
              - Zibi wanted to create a MR
              - Issue created https://gitlab.com/sardana-org/sardana/-/issues/1721
              
        - [ ] Users can break Sardana by messing with configuration in Tango DB:
     	    - status of the diagnostics script:
                - [ ] Henrik wanted to register sherlock in the extras catalogue

        - [ ] For feature request [#1344](https://gitlab.com/sardana-org/sardana/-/issues/1344):
            - ask on Tango forum how to realize in the Device Server that an attribute configuration had changed (is it possible to avoid subscribing to the ATTR_CONF_EVENT)
	    
        - [x] Simplify PseudoMotor configuration? [#1702](https://gitlab.com/sardana-org/sardana/-/issues/1702)
            - MAXIV will look into it.
    
3. Overview of current developments / MR distribution

    - Draft: Warn on conflict instead of remove and replace [!1677](https://gitlab.com/sardana-org/sardana/-/merge_requests/1677)
      - MAX IV needs to reproduce, Zibi was not able to.
      - May only be a problem for changes to read/write setting, but not e.g. data type?

    - trigger/gate multiplexor mode https://gitlab.com/sardana-org/sardana/-/issues/1678
      
    - Related (about controller API version specification and validation):
      - https://gitlab.com/sardana-org/sardana/-/issues/1724
      
    - Reconfig macro https://gitlab.com/sardana-org/sardana/-/merge_requests/873
    
    - Fixtures for core tests https://gitlab.com/sardana-org/sardana/-/merge_requests/1474
      - Getting close to review.

4. MR integration and approval rules

   - Ping integrators when there is a new MR for review. Let's see if it works.
   - Next time, discuss further the usage of a "kanban" or similar for tracking development status.


5. Sardana release?

   - Jan-22 milestone will be skipped
   - Next version probably 3.3, released summer 2022
    
6. AOB

   Next meeting 2022-03-31, arranged by Solaris.
