# Minutes for the Sardana Follow-up Meeting - 2023/08/17

Held on Thursday 2023/08/17 at 14:00

Organizer: Johan @ MAX IV

Present:
- Johan @ MAXIV
- Zibi, Oriol, Marti, Jordi @ ALBA
- Teresa @ DESY
- Michal @ Solaris
- Michael, Daniel @ MBI

## Agenda

1. Discussion: Redis Database Interface for Sardana data publishing

   A discussion initiated by ALBA, to find a solution decoupling data
   acquisition from storage/processing. See Zbignew's email on
   2023-07-24.
   
   _Notes:_
   
   Traditional way: data gathered from HW -> Pool -> MS, then MS
   combines data+metadata and storage. Could data be handled in a more
   efficient way bypassing some of the steps.
   
   Investigating how other systems do this. BLISS uses Redis as 
   intermediary storage, kind of a queue. From there anyone could
   consume data, e.g. a SDM system.
   
   There is a branch in sardana called "poc_redis".
   The initial idea is to publish scan data. The POC is working.
   
   Some initial tests have been done, but performance metrics are not
   available yet. May be done for the continuous scan workshop in 
   september.
   
   There is no concrete plan for how to handle things like images
   or metadata yet.

2. Jul23 release

   Still no big improvements ready, motivating a release.
      
   - Bugfixes to continuous scan, and faulty Taurus dependency could
     be included in an upcoming hotfix.
      
   - Use the "next hotfix" label in gitlab if you want to prioritize
     something for a hotfix.
     
   - Some features of SEP20 are underway but not yet ready.

   Next official release will not happen before the status widget is
   ready.

3. SEP20 status

   - Multiple config file support still needs discussion
     https://gitlab.com/sardana-org/sardana/-/issues/1841
     Still pending.
   
   - Environment support also in design. Zibi wants to start with
     documenting the environment variables and work from there.
     Still pending.
     
4. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
      - A Taurus related bug in 3.4 found related to continuous scans, 
        regarding some checks of velocity. MR exists, will be in a hotfix.
      - Waiting for config improvements before converting more beamlines
        to non-numeric ids.
      - Bug in emergency brake feature, due to some confusion in internal
        state updates. Fix is upcoming, but may have to wait until next
        release.
    - DESY
      - Startup in the coming weeks
    - MAXIV
      - Several beamlines upgraded to 3.4
      - Startup
    - MBI Berlin
      - PySP monitor (?) is a nice UI for controlling scans, maybe some 
        features could be added to showscan online. "hasyutils"?
        - Tiling plots instead of overlapping all in one plot
        - Showing current movable position in the plot, and allows 
          clicking the plot (e.g. to move to a given peak)
      - Daniel had some ideas about improving the scanstats representation.
      - Migrated to 3.4.0 + non-numeric ids, and tried the config tools
        a bit.
      - Still an issue that sardana element names are Tango aliases, which
        means there can be name collisions between sardana installations that
        share the same database. Promising solutions have been proposed.
        See https://gitlab.com/sardana-org/sardana/-/issues/998 for the 
        ongoing discussions.
    - SOLARIS
      - Also startup next week
      - Upgraded to 3.4, pretty smooth so far
       
5. Review pending points from the previous meetings

  - From last meeting
  - From previous meetings:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
    - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
      - Reproduced the issue, a MR is upcoming.
     
6. Overview of current developments / MR distribution
   - Review project board
     - [ ] MR !1940 will be reviewed by Zibi
     - [ ] MR !1939 (experiment status widget) is blocked by the previous but more or
       less ready for testing.
     - [ ] MR !1934 (Add reactivity in waitForEvent() in taurus client) needs review
       by Johan.
     - [ ] MR !1927 (Allow moveableoninput access to ctrl layer) ready for review.
     - Daniel made an issue/comment somewhere about config and multiple pools.

7. Sardana workshop in SOLARIS
   - current status of organization
     - Invitations sent out, Tango controls webpage updated
     - Some details to discuss with Zibi, about presentations about different frameworks
     - We should prepare some smaller presentations on specific solutions at different
       labs.

8. Sardana contributions to events and conferences
   - Tango Workshop at ICALEPCS2023
     - Any preparation needed?
   - ICALEPCS2023
     - Overleaf was proposed by Michal as common edit tool, final decission?
     - Official template already available ?
       Yes, available now.
     - No oral presentation specific to Sardana, but there will be a poster
       and paper. It should cover Sardana+Taurus news from the last 4 years
       plus future plans.

9. Review of the oldest 15 issues in the backlog
   - Should we consider automatically closing old tickets after x years of inactivity?

10. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.

   - Next meeting?
     - September 28th, organized by Solaris.
