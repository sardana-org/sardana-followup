# Agenda for the Sardana Follow-up Meeting - 2023/08/17

Held on Thursday 2023/08/17 at 14:00

Organizer: Johan @ MAX IV

## Agenda

1. Discussion: Redis Database Interface for Sardana data publishing

   A discussion initiated by ALBA, to find a solution decoupling data
   acquisition from storage/processing. See Zbignew's email on
   2023-07-24.

2. Jul23 release

   Still pending some hotfixes and the experiment status widget.

3. SEP20 status

   - Multiple config file support still needs discussion
     https://gitlab.com/sardana-org/sardana/-/issues/1841
   
   - Environment support also in design. Zibi wants to start with
     documenting the environment variables and work from there.
          
4. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS
       
5. Review pending points from the previous meetings

  - From last meeting
  - From previous meetings:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
    - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489):
      - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
     
6. Overview of current developments / MR distribution
   - Review project board
      - type hints - see https://gitlab.com/sardana-org/sardana/-/merge_requests/1915

7. Sardana workshop in SOLARIS
   - current status of organization

8. Sardana contributions to events and conferences
   - Tango Workshop at ICALEPCS2023
     - Any preparation needed?
   - ICALEPCS2023
     - Overleaf was proposed by Michal as common edit tool, final decission?
     - Official template already available ?

9. Review of the oldest 15 issues in the backlog

10. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.

   - Next meeting?
