# Agenda for the Sardana Follow-up Meeting - 2025/02/06

To be held on Thursday 2025/02/06 at 14:00

Organizer: Michal @ SOLARIS

Participants: 


## Agenda

1.    Operation highlights, user feedback, issues, etc. - Round table
       - ALBA
       - DESY
       - MAXIV
       - MBI Berlin
       - SOLARIS
	 
2.    Review of pending points from previous meetings
       - sardana-icepap project in the sardana-org.
         Still not done. Roberto will work on that.
       - H5py opening files with "w+" caused corruption with access control lists in storage. "w" works better, but not sure 
         what the consequences is. Probably due to some change in the infrastructure at ALBA. 
         They will test and perhaps suggest a MR to change the default.
       - Testing latest Taurus version, and will be fixed in upcoming patch. There is a bug with using multiple pools. 
       - Hotfix 3.5.1
          - It is still not done, there are three MR related to that, one aproved and the
	        other ones close to it. MaxIV has made them. It is about the controllers state. Jordi and Oriol will try to check the MRs and
            create the Hotfix.

3.    Overview of current developments / MR distribution
       - ?

4.   Jan25 release status
     - Everybody who is interested in having a MR added, should put it in the [Jan25 Milestones.](https://gitlab.com/sardana-org/sardana/-/milestones/15#tab-merge-requests)
     - Responsible: SOLARIS (Manager#1) + DESY (Manager#2)

5.    Review of oldest 15 issues in the backlog
       - Next to review: #175

6.    AOB
       - Sardana events in 2025? 
         - users training, bug sqashing.... ?
       - Next meeting?
