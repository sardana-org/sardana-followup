# Agenda for the Sardana Follow-up Meeting - 2024/07/04

Held on Thursday 2024/07/04 at 14:00

Organizer: Johan @ MAX IV

## Agenda

1. sardana-icepap project in the sardana-org

2. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS

3. Jan24 release status
    - Next release?

4. Sardana Workshop satellite to Tango Meeting
      
5. Review pending points from the previous meetings
    - From previous meeting
      - [ ] tango serialization monitor acquiring timeout
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.

6. Review of the oldest 15 issues in the backlog

7. Overview of current developments / MR distribution. Not time for this point.
    - Sardana configuration improvements (SEP20): status of environment implementation
    - Shutter integration (SEP21).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
    
8. AOB
   - Next meeting?
