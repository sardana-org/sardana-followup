# Minutes for the Sardana Follow-up Meeting - 2024/07/04

Held on Thursday 2024/07/04 at 14:00

Organizer: Johan @ MAX IV

Participants:
- Johan (MAX IV)
- Teresa (DESY)
- Michal (SOLARIS)
- Jordi (ALBA)
- Oriol (ALBA)
- Vanessa (MAX IV)
- Zibi (ALBA)
- Mirjam (MAX IV)


## Agenda

1. sardana-icepap project in the sardana-org
   - Nobody involved was present

2. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
      - Starting to move to conda, getting away from python 3.5
      - Upgrading one beamline to Sardana 3.5 to test
    - DESY
      - Updating to Debian 12, no other news
    - MAX IV
      - Problem with latency time not being applied to measurement groups (#1975, !2020)
    - MBI Berlin
      - Nobody present
    - SOLARIS
      Some new issues reported:
      - Problem with logging to stdout (#1973)
      - Scanstats ideas (#1974)

3. Jan24 release status
    - Next release? Jan25!
    - "Lower the bar" for releasing "hotfix" versions.
    - Consider moving some things (e.g. config tool) into separate packages?

4. Sardana Workshop satellite to Tango Meeting
    - Notes: https://gitlab.com/sardana-org/sardana-followup/-/blob/main/20240530-SOLEIL/MINUTES.md
    - Follow-up:
      - Documentation improvements / user training?
      - Hold another "bug squashing party"? Virtual or physical?
      
5. Review pending points from the previous meetings
    - NOBUGS papers!
    - From previous meeting
      - [x] tango serialization monitor acquiring timeout
        Solaris has made a workaround for now.
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked. (They will do this for Taurus first)
    - Can we drop support for some old python versions?
      3.5 can be dropped in next release, but what about 3.6? 
      Solaris may be still stuck on 3.6 due to CentOS7.

6. Review of the oldest 15 issues in the backlog
    Next up: issue #150

7. Overview of current developments / MR distribution. Not time for this point.
    - Sardana configuration improvements (SEP20): 
      - Status of environment implementation - Needs documentation! Zibi will do this verrry soon!
      - 
    - Shutter integration (SEP21).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
      Maybe needs integration with Taurus? Should open Taurus issue about it.
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
      Seems like a useful idea, but the solution proposed by dschick in the issue could be enough?
    - Multiple Synchronization Descriptions (#2019)
    - Scanstats ideas (#1974)
    - Icepap continuous mesh scan report from MAX IV, apperars there may be some limitations in the IcePAP performance when running two motors in sync. Will discuss with ALBA and ESRF.
    
8. AOB
   - Next meeting: 2024 august 22, chaired by Solaris.
