# Minutes from the Sardana Follow-up Meeting - 2023/03/30

Held on Thursday 2023/03/30 at 14:00

Organizer: Michal @ SOLARIS

## Participants
    Johan, Zibi, Teresa, Michal, Jordi, Yimeng

## Agenda

1. Jan23 Release
    - status report
      - process of manual tests is still in progress (we wait for Debian10)
      - we agreed to skip Windows tests
      - there are small issues in sardana config - Johan will look on them
      - spock prompt issue is back
      - what's new entry still missing - Zibi will do it
      - hopefully release will be ready before Easter

- Extra 1 Recorders issue (MAX IV)
    - issue: is it possible to differentiate recorder for the combination of scans and create one master file to aggregate smaller files
    (keep reference to small files)
        - for the combination of scans, one can play with recorder environment variable to activate or deactivate different recorders accordingly
        - for the passing reference to files to a given recorder: maybe custom data is a solution? for external links probably not
        - [comments to scan header](https://gitlab.com/sardana-org/sardana/-/issues/771)
    
- Extra 2 Scangui demo (MAX IV)
    - standard GUI in MAX IV
    - many forks (one in SOLARIS)
    - maybe it is worth to integrate this GUI to some extent with Sardana (or at least particular widgets) 
    - MAX IV will make it visible on their public Gitlab
   
2. Operation highlights, user's feedback, issues, etc. - Round table
   
    - SOLARIS
      - no issues
    - MAXIV
      - no issues
    - DESY
      - no issues
    - ALBA
      - MacroServer DB file corrupted (already happened twice)
        - potentially related [issue](https://gitlab.com/sardana-org/sardana/-/issues/1824) 
        - maybe reason is keeping this file on nfs?

3. Review pending points from the previous meetings

  - From last meeting:
    - Problems with aborting macros with Ctrl-C, seemingly caused by Taurus GUIs subscribing to elements. At least, the problems went away when some GUI was killed. The GUI was in particular polling the Door state.
        Potentially related issue https://gitlab.com/sardana-org/sardana/-/issues/1431
        Sardana logs provided by Teresa.
      - Zibi commented on issue [#693](https://gitlab.com/sardana-org/sardana/-/issues/693#note_1293102014)
    - Problem with hangs on moves were reproduced on one BL, running with Tango 7 (!). This was related to GC in taurus, leading to a deadlock. Solution may be to replace Taurus with a normal proxy, or just upgrade Tango. Further investigation needed.
      - no issue created yet
  - From previous meetings:
    - [ ] Zibi will provide diagrams on what controllers should return in `ReadOne()` - requested by MAXIV
      - Jordi works on it - investigation done and short demo presented
      - some more discussion needed
      - where to document it? docstring of `ReadOne()` or in tutorial? easier in the tutorial
      - it would be nice to have at least minimal example in this release - Jordi will work on it 
      - [Related issue](https://gitlab.com/sardana-org/sardana/-/issues/1831)
        - when hardware is ready it should be an empty list
        - when we have issue with hardware it should be an exception
        - nevertheless sardana should handle exceptions as soon as possible and at last in the recorder to avoid having a scan that never ends
    - [x] Wojciech will answer the comments in th MR (Pool should not log an ERROR if hkl is not installed - #394)
      - He promised to look at this MR tomorrow.
    - PseudoCounter in flyscan
      - [ ] Zibi will at least comment of what exactly is needed
       - No update x2
     
4. Overview of current developments / MR distribution
   - Review project board
   - SEP20 related - Johan will look on them
     - [Avoid false positives - MR ready](https://gitlab.com/sardana-org/sardana/-/issues/1852)
     - [tango_host is not protecting agains loading to a different DB](https://gitlab.com/sardana-org/sardana/-/issues/1851)
     - one more issue to come - more investigation needed
   - [Exception raised from AddDevice controller method is IGNORED](https://gitlab.com/sardana-org/sardana/-/issues/637)
     - no strong opinions
     - but better to go with second design as it is more consistent with controller exceptions raised during controller's initialization,
     and it has no drawback of design one
   - [qtspock doesn't support jupyter_client >= 6.1.13](https://gitlab.com/sardana-org/sardana/-/issues/1849)
     - not perfectly solved 
     - as it has minimal impact - for now version is limited
   - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489)
   - [No update of element state until all elements participating in action finish](https://gitlab.com/sardana-org/sardana/-/issues/1818)

5. Sardana workshop in SOLARIS
   - feedback and short discussion
     - to be on the safe side (Tango Meeting), event will take place 11-12.07.2023
     - there is interest in participation
     - Michal and Zibi will work to prepare invitations

6. Review of the oldest 15 issues in the backlog
   - skipped

7. AOB
   - Next meeting?
    - 11.05.2022 at 14:00, organized by ALBA (Zibi)
      - interesting fact: the order of the institutes is alphabetical!