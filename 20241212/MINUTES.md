# Minutes for the Sardana Follow-up Meeting - 2024/12/12

Held on Thursday 2024/12/12 at 14:00

Organizer: Johan @ MAX IV

Participants: 
- Joyhan @ MAX IV
- Oriol @ ALBA
- Michal @ Solaris
- Teresa @ DESY


## Agenda

1.    Operation highlights, user feedback, issues, etc. - Round table
       - ALBA
         + H5py opening files with "w+" caused corruption with access control lists in storage. "w" works better, but not sure what the consequences is. Probably due to some change in the infrastructure at ALBA. They will test and perhaps suggest a MR to change the default.
         + Tango 10 testing caused issue with one motor controller, where the stored value for memorized value in the dial pos was stored in a corrupted way, not as a simple number. This caused issues with loading.
         + Testing latest Taurus version, and will be fixed in upcoming patch. There is a bug with using multiple pools.
       - DESY
         + Nothing
       - MAXIV
         + Nothing
       - MBI Berlin
         + Not represented
       - SOLARIS
         + Nothing
	 
2.    Review of pending points from previous meetings
       - MR proposed by SOLARIS: [!2022](https://gitlab.com/sardana-org/sardana/-/merge_requests/2022)
         Oriol will check it. If not merged soon they will create a patch for running at Solaris.
       - sardana-icepap project in the sardana-org.
         Still not done. Roberto will work on that.

3.    Overview of current developments / MR distribution
       - Hotfix 3.5.1
         It is still not done, there are three MR related to that, one aproved and the
	        other ones close to it. MaxIV has made them.
            Johan will look at them
	     It is about the controllers state. Jordi and Oriol will try to check the MRs and
            create the Hotfix..
       - Sardana configuration improvements (SEP20)
         + There is one MR related to it ([!2036](https://gitlab.com/sardana-org/sardana/-/merge_requests/2036)) fixing a problem found with MG. Review is pending.
         + Johan to take a look.
     	 + Another old MR ([!1989](https://gitlab.com/sardana-org/sardana/-/merge_requests/1989)) is also pending, and ([!2025](https://gitlab.com/sardana-org/sardana/-/merge_requests/2025)) needs some discussion. Alba will take care.
       - Shutter integration (SEP21)
         + Discussions following the MAX IV visit in november, ongoing.
       - Fix invalid escape sequence warnings [!2028](https://gitlab.com/sardana-org/sardana/-/merge_requests/2028)).
       - entryname and dataname in showscan customizable ([!2018](https://gitlab.com/sardana-org/sardana/-/merge_requests/2018))
         Reviewed by Oriol. It is ready to merge when documented.
       - Set/reset ScanID from expconf ([#1967](https://gitlab.com/sardana-org/sardana/-/issues/1967)), ([MR](https://gitlab.com/sardana-org/sardana/-/merge_requests/2037))
         + Created by Wojciech. He had some doubts about what ScanID should be, the current one or the new one. It was decided to set in the GUI the ScanID corresponding to the next scan,
	 this will be explicitly written in the GUI, since the ScanID environment variable
	 indicates actually the ID of the last scan.
       - Add "epoch" (#E) in SPEC recorder header ([!1356](https://gitlab.com/sardana-org/sardana/-/merge_requests/1356))
         + It is an old MR. In spec #E is in the file header, but there is not file header
	 in the sardana files, but only scan header. It has to be discussed.
	 Perhaps can be added as a custom field in the scan header.
	 The way implemented in the MR is not wanted, the MR will be kept open,
	 but the implementation has to be changed.
         + This recorder is used at ALBA, DESY and maybe also at Solaris
       - Wojtek working on the sardana config environment support (MR HERE)
 

4.   Jan25 release
     Everybody who is interested in having a MR added, should put it in the Jan25 Milestones.
     The Hotfix 3.5.1 will be still done independently before the release.
     Responsible: DESY + Solaris
     

5.    Review of oldest 15 issues in the backlog
       - Next to review: #175

6.    AOB
       - Next meeting?
         2025-02-06 at 14:00, arranged by Solaris
         
