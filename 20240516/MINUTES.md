# Minutes for the Sardana Follow-up Meeting - 2024/05/16

Held on Thursday 2024/05/16 at 15:00

Organizer: Teresa @ DESY

Particitpants: Michal (Solaris), Oriol, Jordi, Zibi (Alba), Vanessa, Carla, Johan (MaXIV), Michael - later (MBI), Teresa (DESY)

## Minutes

1. sardana-icepap project in the sardana-org
    Zibi communicate the sardana-icepap developers that it was decided to go
    on with the migration. Roberto (Alba) will contact the rest of the people
    involved (MaxIV, Solaris, Alba). Vanessa talked to Marcel and he will
    follow up.


2. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA. No issues
    - DESY. Ctrl-C problems, Sardana has other ideas for refactoring
      and not doing the on_stop, on_abort calls, also not use reserve elements.
      They are trying to improve the implementation. An issue with the
      logs of MacroServer and Pool will be created if the problem arise again.
    - MAXIV. Testing and using the latest Release (the one coming),
      also for PyTango. In one beamline they what an issue due to different
      controllers with attributes with the same name but different
      characteristics. Something changed in Tango cpp. Sardana uses a
      hack there. Tango can not have attributes with the same name
      and different characteristics. They do not know the cause,
      but they solve it going to the previous version of PyTango.
      This fact will be remembered in the sardana talk in the Tango meeting.
      It is about the fully dinamic attributes. Henry in MAXIV did
      something about it but it was stopped. Sardana will benefit a lot
      if Tango improves this. The solution now is to avoid this in Sardana.
    - MBI Berlin. Not present at this point
    - SOLARIS. No issues

3. Jan24 release status.
   The release process started, there are many plataforms to test.
   MaXIV is done with the conda tests. There were some small issues,
   but nothing is wrong.
   Debian10 is stared by Alba, they are setting up the system.
   CentoS, debian11 and 12 will start soon. debian11 will be probably skipped.

4. Status Sardana Workshop satellite to Tango Meeting
   17 people are registered to participate on-site. The agenda is ready:
   https://gitlab.com/sardana-org/sardana-followup/-/blob/main/20240530-SOLEIL/AGENDA.md?ref_type=heads
   Items of the agenda were assigned:
   2. Johan
   3. Vanessa, Jordi
   4. Oriol (possible also Jan Kotanski)
   6. Zibi, Michal
   7. Michael
   8. Feedback from Johan, Jordi, ... Jordi will prepare some material
      for openning discussion
   9. There is a roadmap in the documentation and it will be discussed
   

5. Tango Meeting at SOLEIL - Status possible sardana talk
    - Call for volunteers as speakers
    Google slides will be shared by Zibi, they are almost ready.
    They have the contents of the WS but less material.
    The people with the different topics could
    prepare the slide with the corresponding topic.
    Zibi will probably present it.

6. Review pending points from the previous meetings. Not time for this point.
    - From previous meeting
      - [ ] tango serialization monitor acquiring timeout
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.

7. Review of the oldest 15 issues in the backlog. Not time for this point.

8. Overview of current developments / MR distribution. Not time for this point.
    - Sardana configuration improvements (SEP20): status of environment implementation
    - Shutter integration (SEP21).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
    
9. AOB
    - Next meeting 04/07/24 at 14:00 (organized by MaXIV)