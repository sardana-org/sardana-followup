# Agenda for the Sardana Follow-up Meeting - 2024/05/16

To be held on Thursday 2024/05/16 at 15:00

Organizer: Teresa @ DESY

## Agenda

1. sardana-icepap project in the sardana-org
    - Status: Communicate sardana-icepap developers to go on with the migration ?,

2. Operation highlights, user's feedback, issues, etc. - Round table
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS

3. Jan24 release status

4. Status Sardana Workshop satellite to Tango Meeting

5. Tango Meeting at SOLEIL - Status possible sardana talk
    - Call for volunteers as speakers

6. Review pending points from the previous meetings
    - From previous meeting
      - [ ] tango serialization monitor acquiring timeout
      - [ ] Sardana Users Training. ALBA wanted to  prepare a survey for their customer units and share with MAX-IV and SOLARIS so something similar can be asked.

7. Review of the oldest 15 issues in the backlog

8. Overview of current developments / MR distribution
    - Sardana configuration improvements (SEP20): status of environment implementation
    - Shutter integration (SEP21).
    - Add PickleProtocol property - [!1984](https://gitlab.com/sardana-org/sardana/-/merge_requests/1984) 
    - Run measurement sequence in a more flexible way - [#1942](https://gitlab.com/sardana-org/sardana/-/issues/1942)
    
9. AOB
    - Next meeting?