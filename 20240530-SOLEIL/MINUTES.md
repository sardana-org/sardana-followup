# Sardana Workshop satellite to Tango Meeting 2024 at SOLEIL held on 30/05/2024
 
## Minutes

1. Welcome and Warm-up: 13:45 - 14:00
   
2. [Sardana Configuration Format and Tools](https://sardana-controls.org/users/configuration/index.html) ([SEP20](https://gitlab.com/sardana-org/sardana/-/blob/develop/doc/source/sep/SEP20.md)): 14:00 - 14:30 
   - Linus asked how the tool would deal with measurement groups in case one *disables*
     them by commenting them out in the file, would it delete them from the system.
     Probably the use of the tools and format with the measurement groups is not
     yet so deeply thought how useful it will be for configuring them.
     We need to see how it works in real use case scenarios.
   - Zibi raised a question to the Community if anyone is waiting for reintroduction
     of the `renameelem` macro. 
     There is no such need. Anton even explainted the last time
     they used it at MAXIV, before migrating to non-numerical ids, it was not working correctly.

3. Documentation and User's Training: 16:00 - 16:30
   - Jordi commented that the best documentation for the users is the one which is incorported
     in the tools they use e.g. visible for them e.g. tooltips, hints, etc., help in spock,
     or better error messages even with the links to the docs.
   - Mirjam comments that the time-boxed projects with a clear objectives to improve documentation
     could be a better approach than documentation camps. 
   - Michael confirms that the "Extra projects" catalog is very useful for newcomers.    
  
4. Data publishing in Redis: 15:00 - 15:30 
   - Both ALBA and DESY presented their PoC of integration of sardana with Redis using blissdata.    

5. Coffee break: 15:30 - 16:00

6. Continuous Scans: 16:00 - 16:30
   - Zibi quickly evalutated the code modifications that Vanessa presented and all
     make sense and we need to evaluate them more cerefully soon to be integrated in Sardana.
   - Discussion on multimodal/multitechnique scans as currently implemented at MAXIV
     - Synchronization is done with PandABox.
     - Motion controller is loaded with a single motion trajectory program and started once.
     - Detectors are prepared once and started once.
     - Data is interpreted independently for each technique afterwards.
   - Looks like both the multiple synchronization descritpions and publishing in Redis could be
     be very benefitial for multimodal scans like the ne presented by Vanessa.

7. [Showscan online improvements](https://gitlab.com/sardana-org/sardana/-/issues/1913): 16:30 - 16:45
   - Long time ago requested features, already available in third party tools, for example, at DESY with psmonitor.
   - In the future, if we decide to support publishing data in Redis, or similar, then
     tools like Flint, which access data directly from the database would somehow overlap
     with the showscan online. But still they will have value because it uses a simpler architecture
     (no need to deploy Redis).

8. Experiment Status widget: 16:45 - 17:00
   - Michael asked if it was necessary to open the expstatus GUI before the start of the macro.
     The answer is no, the widget is capable to populate itself while the macro is running by
     reading the ReservedElements attribute of the Door, but one has to press the *refresh* button.

9. Roadmap Review and Open Discussion: 17:00 - 17:45
   - Vanessa comments that it would be nice to add motion trajectories to the roadmap.
   - Zibi comments that it is a feature that it would be benefitial to many institutes.
     Maybe we could start it as soon we finish one of the big developments we have now in
     progress: data publishing and multiple synchronization descriptions.
   - Alexander asked how to implement long actions done on elements. When writing a controller
     one can not define a commands, just attributes. Commands could be eventually added, but they
     can not execute long actions, this would neeed to be done in background with state control.
     Michael comments on the possibility to use macros e.g. homing macros.

10.  Summary and Good Bye: 17:45 - 18:00
    - After the quick summary we had an oportunity to comment with Thomas B. and Thomas J. 
      about the dynamic attributes per device. 
