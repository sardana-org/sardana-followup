# Sardana Workshop satellite to Tango Meeting 2024 at SOLEIL

## Organizers

- ALBA
- DESY
- MAXIV
- MBI-Berlin
- SOLARIS

## Remote participation

Join Zoom Meeting
https://rediris.zoom.us/j/96249616662?pwd=QUxxNFZ4ems4dTF0VnBSUmtUaWpMdz09
 
Meeting ID: 962 4961 6662
Passcode: 275716
 
## Agenda

1. Welcome and Warm-up: 13:45 - 14:00
   
2. [Sardana Configuration Format and Tools](https://sardana-controls.org/users/configuration/index.html) ([SEP20](https://gitlab.com/sardana-org/sardana/-/blob/develop/doc/source/sep/SEP20.md)): 14:00 - 14:30 
   - Demos of typical use cases including split of configuration in multiple files
   - Future enhancements and next steps: MacroServer environment, ...

3. Documentation and User's Training: 16:00 - 16:30
   - Discussion on different ideas on how to improve documentation e.g.
     - Merge user and developer documentation to avoid fragmentation of docs
     - Improvements of macros' documentation e.g. autodoc preprocessing hooks to document parameters etc.
     - Type hints as API documentation
     - User's tutorial based on exercices in the docs (as an example see [cmake's tutorial](https://cmake.org/cmake/help/latest/guide/tutorial/index.html))
  
4. Data publishing in Redis: 15:00 - 15:30 
   - Publishing data in Redis using a Sardana [recorder]((https://github.com/ALBA-Synchrotron/sardana-redis)) based on [blissdata](https://pypi.org/project/blissdata/) from the ESRF's [Bliss](https://gitlab.esrf.fr/bliss/bliss) project.
   - Publishing data in Redis directly from the sardana core on the Pool.

5. Continuous Scans: 14:30 - 15:00
   - Refactorings and optimizations in Generic Scan Framework and scan macros e.g. `meshct`, etc.
   - Shutter - new element in Sardana and its use in scans [SEP21](https://gitlab.com/sardana-org/sardana/-/merge_requests/1995)
   - Non-equal acquisition pace of channels and diffrent synchronization descriptions


6. Coffee break: 15:30 - 16:00

7. [Showscan online improvements](https://gitlab.com/sardana-org/sardana/-/issues/1913): 16:30 - 16:45

8. Experiment Status widget: 16:45 - 17:00
   - Demos of typical use cases
   - Next steps

9.  Roadmap Review and Open Discussion: 17:00 - 17:45
   - Review progress of the roadmap prepared prior the ICALEPCS2023
   - Ideas of topics for Open Discussion:
     - Python version deprecation policy https://gitlab.com/sardana-org/sardana/-/issues/1914
     - Experiences in using Sardana with conda in production
     - Testing strategies: [attempts to use MultiDeviceTestContext](https://gitlab.com/sardana-org/sardana/-/issues/1951) and [core tests fixtures](https://sardana-controls.org/devel/api/sardana/pool/test.html#module-sardana.pool.test)
     - sardana-org members grooming
     - ...
  
10.  Summary and Good Bye: 17:45 - 18:00
