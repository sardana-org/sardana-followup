# Agenda for the Sardana Follow-up Meeting - 2023/09/28

Held on Thursday 2023/09/28 at 14:00

Organizer: Michal @ SOLARIS

## Agenda

1. Sardana Continuous Scans Workshop @ SOLARIS
    - summary and conclusions
    - blissdata 1.0 demo poll

2. ICALEPCS2023 contribution
    - status update
    - help needed?

3. Jul23 release status

4. SEP20 status

5. Operation highlights, user's feedback, issues, etc. - Round table
   
    - ALBA
    - DESY
    - MAXIV
    - MBI Berlin
    - SOLARIS
       
6. Review pending points from the previous meetings

  - From last meeting
  - From previous meetings:
    - [ ] Scan GUI: MAX IV will make it visible on their public Gitlab
    - [Experiment status widget](https://gitlab.com/sardana-org/sardana/-/issues/1489):
      - [ ] Michal will make a MR for the `wa` macro with regard to reserved elements
        - Reproduced the issue, a MR is upcoming.
     
7. Overview of current developments / MR distribution
   - Review project board
      - ...

8. Review of the oldest 15 issues in the backlog

9. AOB
   - Question on tango Slack: Can we integrate https://gitlab.com/sardana-org into the tango-controls group on gitlab? This would help in seeing activity and also enable gitlab search tango-controls wide.
     - Needs discussion of pros and cons.
   - Next meeting?
